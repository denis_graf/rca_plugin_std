/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.rca_plugin_std.rca_plugin_java.remocons;

import android.view.View;
import android.widget.Button;

import com.github.rosjava.android_remctrlark.rcajava.configuration.IJavaRemoConConfiguration;
import com.github.rosjava.android_remctrlark.rcajava.context.misc.IOnRemoCompLaunchedListener;
import com.github.rosjava.android_remctrlark.rcajava.context.misc.IRemoCompProxy;
import com.github.rosjava.android_remctrlark.rcajava.misc.ICheckedRunnable;
import com.github.rosjava.android_remctrlark.rcajava_impl.AJavaRemoConImplBase;
import com.github.rosjava.rca_plugin_std.rca_plugin_java.R;

public class RosTeleop extends AJavaRemoConImplBase {

	private static enum Visualization {
		CAMERA,
		MAP,
	}

	private static final String REMOCOMP_TYPE_MAP = "ros_map";
	private static final String REMOCOMP_TYPE_CAMERA = "ros_image";
	private static final String REMOCOMP_ID_VISUALIZATION = "visualization";

	private Visualization mSwitchVisualizationTo = null;

	@Override
	public void createUI() {
		getLayout().getInflater().inflate(R.layout.ros_teleop, getParentView());
	}

	@Override
	public void start(IJavaRemoConConfiguration configuration) {
		final Button switchVisualizationButton = (Button) getParentView().findViewById(R.id.switch_visualization);
		switchVisualizationButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getRcaContext().postToImplThread(new ICheckedRunnable() {
					@Override
					public void run() {
						if (mSwitchVisualizationTo == Visualization.MAP) {
							getRelauncher().setType(REMOCOMP_ID_VISUALIZATION, REMOCOMP_TYPE_MAP);
						} else {
							getRelauncher().setType(REMOCOMP_ID_VISUALIZATION, REMOCOMP_TYPE_CAMERA);
						}
						getRelauncher().commit();
					}
				});
			}
		});

		final IRemoCompProxy visualizationProxy = getRcaContext().getRemoCompProxy(REMOCOMP_ID_VISUALIZATION);
		visualizationProxy.setOnLaunchedListener(new IOnRemoCompLaunchedListener() {
			@Override
			public void onRemoCompLaunched(IRemoCompProxy proxy) {
				if (proxy.getType().equals(REMOCOMP_TYPE_CAMERA)) {
					mSwitchVisualizationTo = Visualization.MAP;
					switchVisualizationButton.post(new Runnable() {
						@Override
						public void run() {
							switchVisualizationButton.setText("show map");
						}
					});
				} else {
					mSwitchVisualizationTo = Visualization.CAMERA;
					switchVisualizationButton.post(new Runnable() {
						@Override
						public void run() {
							switchVisualizationButton.setText("show camera");
						}
					});
				}
			}
		});
	}
}
