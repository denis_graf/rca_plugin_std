/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.rca_plugin_std.rca_plugin_java.remocomps;

import com.github.rosjava.android_remctrlark.rcajava.binders.annotations.RcaType;
import com.github.rosjava.android_remctrlark.rcajava.configuration.IJavaRemoCompConfiguration;
import com.github.rosjava.android_remctrlark.rcajava_impl.AJavaRemoCompImplBase;
import com.github.rosjava.rca_plugin_std.rca_plugin_java.R;

import org.ros.android.view.VirtualJoystickView;

@RcaType("This RemoComp wraps VirtualJoystickView written by Munjal Desai (munjaldesai@google.com):\n" +
		"VirtualJoystickView creates a virtual joystick view that publishes velocity " +
		"as (geometry_msgs.Twist) messages. The current version contains the following " +
		"features: snap to axes, turn in place, and resume previous velocity.\n" +

		"\nPublished topics:\n" +
		"\t" + RosVirtualJoystick.TOPIC_NAME_CMD_VEL + " (geometry_msgs/Twist): Current velocity.\n" +

		"\nSubscribed topics:\n" +
		"\t" + RosVirtualJoystick.TOPIC_NAME_ODOM + " (nav_msgs/Odometry): Odometry used for visualizing robot's orientation when rotating.\n" +

		"\nNOTE: Current implementation causes memory leaks when relaunching. The current workaround for that issue " +
		"forces the RemoCon to relaunch and reconnect to ROS master.")
public class RosVirtualJoystick extends AJavaRemoCompImplBase {

	public static final String TOPIC_NAME_CMD_VEL = "cmd_vel";
	@SuppressWarnings("UnusedDeclaration")
	public static final String TOPIC_NAME_ODOM = "odom";  // Set internally in VirtualJoystickView.

	private VirtualJoystickView mVirtualJoystick = null;
	private boolean mStarted = false;

	@Override
	public void createUI() {
		getLayout().getInflater().inflate(R.layout.ros_virtual_joystick, getParentView());
		mVirtualJoystick = (VirtualJoystickView) getParentView().findViewById(R.id.virtual_joystick_view);
	}

	@Override
	public void start(IJavaRemoCompConfiguration configuration) {
		// Note: For avoiding memory leaks, we have to restart ROS when this RemoComp is relaunched.
		getRcaContext().requestRemoConRebootOnRelaunch(true);

		mVirtualJoystick.setTopicName(TOPIC_NAME_CMD_VEL);
		getNodeExecutor().execute(mVirtualJoystick, configuration.createNodeConfiguration(getRcaContext().getType()));
		mStarted = true;
	}

	@Override
	public void shutdown() {
		if (mStarted) {
			getNodeExecutor().shutdownNodeMain(mVirtualJoystick);
			mVirtualJoystick = null;
			mStarted = false;
		}
	}
}
