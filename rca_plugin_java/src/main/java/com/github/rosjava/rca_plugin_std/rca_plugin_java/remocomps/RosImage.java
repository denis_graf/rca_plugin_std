/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.rca_plugin_std.rca_plugin_java.remocomps;

import android.graphics.Bitmap;
import android.widget.ImageView;

import com.github.rosjava.android_remctrlark.rcajava.binders.annotations.RcaType;
import com.github.rosjava.android_remctrlark.rcajava.configuration.IJavaRemoCompConfiguration;
import com.github.rosjava.android_remctrlark.rcajava.error_handling.RcaLayoutException;

import org.ros.android.BitmapFromCompressedImage;
import org.ros.android.message_processor.AsyncMessageProcessorNodeMain;
import org.ros.android.message_processor.MessageProcessorNodeMain;
import org.ros.namespace.GraphName;

@RcaType("This RemoComp consists of a image view listening for image messages. The last message is shown in the image view.\n" +

		"\nParameters:\n" +
		"\t" + RosImage.PARAM_NAME_VIEW_CLASS + " (str): Sets the class to inflate. " +
		"It must be ImageView or a subclass of it. " +
		"Default: " + RosImage.PARAM_DVALUE_VIEW_CLASS + "\n" +
		"\t" + RosImage.XML_NS_ANDROID + "/<attr> (any): Sets the attribute <attr> of the view to inflate.\n" +

		"\nSubscribed topics:\n" +
		"\t" + RosImage.TOPIC_NAME + " (sensor_msgs/CompressedImage): Image to display in the image view.")
public class RosImage extends ARosViewRemoComp {

	public static final String TOPIC_NAME = "image_raw/compressed";
	public static final String PARAM_NAME_VIEW_CLASS = "ImageViewClass";
	public static final String PARAM_DVALUE_VIEW_CLASS = "ImageView";

	private static final String VIEW_ID = "image_view";

	private MessageProcessorNodeMain<sensor_msgs.CompressedImage> mRosNode = null;
	private ImageView mView = null;

	@Override
	public void createUI() throws RcaLayoutException {
		mAndroidAttrs.put("src", "@com.github.rosjava.android_remctrlark:drawable/icon");
		final String viewClass = readConfig(PARAM_DVALUE_VIEW_CLASS, PARAM_NAME_VIEW_CLASS, VIEW_ID);
		final String layoutXmlString = createLayoutXmlString(viewClass);
		mView = (ImageView) inflateLayoutFromXmlString(layoutXmlString, VIEW_ID);
	}

	@Override
	public void start(IJavaRemoCompConfiguration configuration) throws NoSuchMethodException {
		if (mView == null) {
			// This can happen, if there were errors during creation of the view.
			return;
		}

		mRosNode = new AsyncMessageProcessorNodeMain<Bitmap, sensor_msgs.CompressedImage>(sensor_msgs.CompressedImage._TYPE, TOPIC_NAME, GraphName.of(getRcaContext().getType()));
		bindViewPropertyToRos(mRosNode, mView, "setImageBitmap", Bitmap.class, new BitmapFromCompressedImage());

		getNodeExecutor().execute(mRosNode, configuration.createNodeConfiguration(mRosNode.getDefaultNodeName()));
	}

	@Override
	public void shutdown() {
		if (mRosNode != null) {
			getNodeExecutor().shutdownNodeMain(mRosNode);
			mView = null;
			mRosNode = null;
		}
	}
}
