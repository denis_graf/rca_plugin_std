/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.rca_plugin_std.rca_plugin_java.remocomps;

import com.github.rosjava.android_remctrlark.rcajava.binders.annotations.RcaType;
import com.github.rosjava.android_remctrlark.rcajava.configuration.IJavaRemoCompConfiguration;
import com.github.rosjava.android_remctrlark.rcajava_impl.AJavaRemoCompImplBase;
import com.github.rosjava.rca_plugin_std.rca_plugin_java.R;

import org.ros.android.view.visualization.VisualizationView;
import org.ros.android.view.visualization.layer.CameraControlLayer;
import org.ros.android.view.visualization.layer.LaserScanLayer;
import org.ros.android.view.visualization.layer.OccupancyGridLayer;
import org.ros.android.view.visualization.layer.PathLayer;
import org.ros.android.view.visualization.layer.PosePublisherLayer;
import org.ros.android.view.visualization.layer.PoseSubscriberLayer;
import org.ros.android.view.visualization.layer.RobotLayer;

@RcaType("This RemoComp wraps a layer composition using VisualizationView based on org.ros.android.android_tutorial_teleop " +
		"written by Munjal Desai (munjaldesai@google.com), Damon Kohler (damonkohler@google.com) and Lorenz Moesenlechner " +
		"(moesenle@google.com) demonstrating how to use some of views from the rosjava android library.\n" +

		"\nParameters:\n" +
		"\t" + RosMap.PARAM_NAME_MAP_FRAME + "\n" +
		"\t" + RosMap.PARAM_NAME_ROBOT_FRAME + "\n" +
		"\t" + RosMap.PARAM_NAME_CAMERA_FRAME + "\n" +

		"\nPublished topics:\n" +
		"\t" + RosMap.TOPIC_NAME_GOAL_POSE + " (geometry_msgs/PoseStamped)\n" +

		"\nSubscribed topics:\n" +
		"\t" + RosMap.TOPIC_NAME_MAP + "\n" +
		"\t" + RosMap.TOPIC_NAME_PLAN + "\n" +
		"\t" + RosMap.TOPIC_NAME_DYN_PLAN + "\n" +
		"\t" + RosMap.TOPIC_NAME_SCAN + "\n" +
		"\t" + RosMap.TOPIC_NAME_TF + "\n" +
		"\t" + RosMap.TOPIC_NAME_GOAL_POSE + " (geometry_msgs/PoseStamped)\n" +

		"\nNOTE: Current implementation causes memory leaks when relaunching. The current workaround for that issue " +
		"forces the RemoCon to relaunch and reconnect to ROS master.")
public class RosMap extends AJavaRemoCompImplBase {

	public static final String PARAM_NAME_MAP_FRAME = "robot_frame";
	public static final String PARAM_NAME_ROBOT_FRAME = "robot_frame";
	public static final String PARAM_NAME_CAMERA_FRAME = "camera_frame";

	public static final String TOPIC_NAME_MAP = "map";
	public static final String TOPIC_NAME_PLAN = "move_base/NavfnROS/plan";
	public static final String TOPIC_NAME_DYN_PLAN = "move_base_dynamic/NavfnROS/plan";
	public static final String TOPIC_NAME_SCAN = "scan";
	public static final String TOPIC_NAME_GOAL_POSE = "simple_waypoints_server/goal_pose";
	@SuppressWarnings("UnusedDeclaration")
	public static final String TOPIC_NAME_TF = "tf";  // Set internally.

	private VisualizationView mVisualizationView = null;
	private boolean mStarted = false;

	@Override
	public void createUI() {
		getLayout().getInflater().inflate(R.layout.ros_map, getParentView());
		mVisualizationView = (VisualizationView) getParentView().findViewById(R.id.visualization);
	}

	@Override
	public void start(IJavaRemoCompConfiguration configuration) {
		// Note: For avoiding memory leaks, we have to restart ROS when this RemoComp is relaunched.
		getRcaContext().requestRemoConRebootOnRelaunch(true);

		mVisualizationView.addLayer(new CameraControlLayer(getApkContext(), getNodeExecutor().getScheduledExecutorService()));
		mVisualizationView.addLayer(new OccupancyGridLayer(TOPIC_NAME_MAP));
		mVisualizationView.addLayer(new PathLayer(TOPIC_NAME_PLAN));
		mVisualizationView.addLayer(new PathLayer(TOPIC_NAME_DYN_PLAN));
		mVisualizationView.addLayer(new LaserScanLayer(TOPIC_NAME_SCAN));
		mVisualizationView.addLayer(new PoseSubscriberLayer(TOPIC_NAME_GOAL_POSE));
		mVisualizationView.addLayer(new PosePublisherLayer(TOPIC_NAME_GOAL_POSE, getApkContext()));

		final String mapFrame = getConfig().getParamString(PARAM_NAME_MAP_FRAME, null);
		if (mapFrame != null) {
			mVisualizationView.getCamera().setFrame(mapFrame);
		}

		final String robotFrame = getConfig().getParamString(PARAM_NAME_ROBOT_FRAME, null);
		if (robotFrame != null) {
			mVisualizationView.addLayer(new RobotLayer(robotFrame));
		}

		final String cameraFrame = getConfig().getParamString(PARAM_NAME_CAMERA_FRAME, null);
		if (cameraFrame != null) {
			mVisualizationView.getCamera().jumpToFrame(cameraFrame);
		}

		getNodeExecutor().execute(mVisualizationView, configuration.createNodeConfiguration(getRcaContext().getType()));
		mStarted = true;
	}

	@Override
	public void shutdown() {
		if (mStarted) {
			getNodeExecutor().shutdownNodeMain(mVisualizationView);
			mVisualizationView = null;
			mStarted = false;
		}
	}
}
