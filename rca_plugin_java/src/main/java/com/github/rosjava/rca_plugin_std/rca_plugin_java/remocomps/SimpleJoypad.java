/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.rca_plugin_std.rca_plugin_java.remocomps;

import android.view.MotionEvent;
import android.view.View;

import com.github.rosjava.android_remctrlark.rcajava.binders.IJavaRemoCompBinderImpl;
import com.github.rosjava.android_remctrlark.rcajava.binders.annotations.RcaMethod;
import com.github.rosjava.android_remctrlark.rcajava.binders.annotations.RcaParam;
import com.github.rosjava.android_remctrlark.rcajava.binders.annotations.RcaReturn;
import com.github.rosjava.android_remctrlark.rcajava.binders.annotations.RcaType;
import com.github.rosjava.android_remctrlark.rcajava.configuration.IJavaRemoCompConfiguration;
import com.github.rosjava.android_remctrlark.rcajava.context.IJavaRemoCompContext;
import com.github.rosjava.android_remctrlark.rcajava.error_handling.RcaLayoutException;
import com.github.rosjava.android_remctrlark.rcajava.misc.ICheckedRunnable;
import com.github.rosjava.android_remctrlark.rcajava_impl.AJavaRemoCompImplBase;
import com.github.rosjava.rca_plugin_std.rca_plugin_java.R;

import org.ros.internal.node.topic.RcaRepeatingPublisher;
import org.ros.namespace.GraphName;
import org.ros.node.ConnectedNode;
import org.ros.node.Node;
import org.ros.node.NodeMain;
import org.ros.node.topic.Publisher;

import geometry_msgs.Twist;

@RcaType("This RemoComp implements a simple joypad with configurable velocity and frequency.\n" +

		"\nParameters:\n" +
		"\t" + SimpleJoypad.PARAM_NAME_VELOCITY + " (float): Velocity, which is simply the value added to the move command. " +
		"Default: 1.0\n" +
		"\t" + SimpleJoypad.PARAM_NAME_FREQUENCY + " (int): Frequency the move commands are published with. " +
		"Default: 1\n" +

		"\nPublished topics:\n" +
		"\t" + SimpleJoypad.TOPIC_NAME_CMD_VEL + " (geometry_msgs/Twist): Current move command.")
public class SimpleJoypad extends AJavaRemoCompImplBase implements View.OnTouchListener {

	public static final String PARAM_NAME_VELOCITY = "velocity";
	public static final String PARAM_NAME_FREQUENCY = "frequency";
	public static final String TOPIC_NAME_CMD_VEL = "cmd_vel";

	@SuppressWarnings("UnusedDeclaration")
	private class ImplBinder implements IJavaRemoCompBinderImpl {
		@RcaMethod("Returns the current velocity.")
		@RcaReturn("Current velocity value.")
		public double getVelocity() {
			return mVelocity;
		}

		@RcaMethod("Sets the velocity, which is simply the value added to the move command")
		public void setVelocity(
				@RcaParam(name = "velocity", value = "new velocity value") Double velocity) {
			if (velocity == mVelocity) {
				if (debug) {
					getLog().d("setting velocity unchanged");
				}
				return;
			}
			if (debug) {
				getLog().d("setting new velocity: " + velocity + " (old value: " + mVelocity + ")");
			}
			final double oldVelocity = mVelocity;
			updateMoveCommandVelocity(velocity);
		}

		@RcaMethod("Returns the current frequency.")
		@RcaReturn("Current frequency value.")
		public int getFrequency() {
			return mFrequency;
		}

		@RcaMethod("Sets the frequency the move commands are published with.")
		public void setFrequency(
				@RcaParam(name = "frequency", value = "New frequency value.") Integer frequency) {
			if (frequency == mFrequency) {
				if (debug) {
					getLog().d("setting frequency unchanged");
				}
				return;
			}
			if (debug) {
				getLog().d("setting new frequency: " + frequency + " (old value: " + mFrequency + ")");
			}
			final int oldFrequency = mFrequency;
			mFrequency = frequency;
			updateMoveCommandVelocity();
		}
	}

	private ConnectedNode mConnectedNode = null;
	private Publisher<Twist> mPublisher = null;
	private RcaRepeatingPublisher<Twist> mRepeatingPublisher = null;

	private double mVelocity = 1.0;
	private int mFrequency = 1;
	private double mAngularZStep = 0.0;
	private double mLinearXStep = 0.0;
	private Twist mCurTwist = null;

	private final ImplBinder mBinder = new ImplBinder();

	private NodeMain mRosNode;
	private View mButtonRight;
	private View mButtonLeft;
	private View mButtonForward;
	private View mButtonBackward;

	@Override
	public void init(IJavaRemoCompContext context) throws Throwable {
		super.init(context);
		mVelocity = getRcaContext().getConfig().getParamDouble(PARAM_NAME_VELOCITY, mVelocity);
		mFrequency = getRcaContext().getConfig().getParamInteger(PARAM_NAME_FREQUENCY, mFrequency);
	}

	@Override
	public IJavaRemoCompBinderImpl createBinder() {
		return mBinder;
	}

	@Override
	public void createUI() throws RcaLayoutException {
		getRcaContext().getLayout().getInflater().inflate(R.layout.simple_joypad, getParentView());
		mButtonRight = getParentView().findViewById(R.id.button_right);
		mButtonLeft = getParentView().findViewById(R.id.button_left);
		mButtonForward = getParentView().findViewById(R.id.button_forward);
		mButtonBackward = getParentView().findViewById(R.id.button_backward);
	}

	@Override
	public void start(IJavaRemoCompConfiguration configuration) {
		final String type = getRcaContext().getType();

		mRosNode = new NodeMain() {
			@Override
			public GraphName getDefaultNodeName() {
				return GraphName.of(type);
			}

			@Override
			public void onStart(ConnectedNode connectedNode) {
				mConnectedNode = connectedNode;
				mPublisher = mConnectedNode.newPublisher(TOPIC_NAME_CMD_VEL, Twist._TYPE);
				mCurTwist = mPublisher.newMessage();

				mButtonRight.setOnTouchListener(SimpleJoypad.this);
				mButtonLeft.setOnTouchListener(SimpleJoypad.this);
				mButtonForward.setOnTouchListener(SimpleJoypad.this);
				mButtonBackward.setOnTouchListener(SimpleJoypad.this);
			}

			@Override
			public void onShutdown(Node node) {
				getRcaContext().runOnImplThread(new ICheckedRunnable() {
					@Override
					public void run() {
						shutdown();
					}
				});
			}

			@Override
			public void onShutdownComplete(Node node) {
			}

			@Override
			public void onError(Node node, Throwable throwable) {
				getRcaContext().handleException(throwable);
			}
		};

		getRcaContext().getNodeExecutor().execute(mRosNode, configuration.createNodeConfiguration(type));
	}

	@Override
	public void shutdown() {
		if (mRosNode != null) {
			getRcaContext().getNodeExecutor().shutdownNodeMain(mRosNode);
			mButtonRight.setOnTouchListener(null);
			mButtonLeft.setOnTouchListener(null);
			mButtonForward.setOnTouchListener(null);
			mButtonBackward.setOnTouchListener(null);
			updateMoveCommandStop();
			mRepeatingPublisher = null;
			mPublisher = null;
			mCurTwist = null;
			mConnectedNode = null;
			mRosNode = null;
		}

		mButtonRight = null;
		mButtonLeft = null;
		mButtonForward = null;
		mButtonBackward = null;
	}

	@Override
	public boolean onTouch(final View view, MotionEvent event) {
		final int action = event.getActionMasked();

		getRcaContext().postToImplThread(new ICheckedRunnable() {
			@Override
			public void run() {
				if (mConnectedNode == null) {
					getLog().e("not connected to ros, no action executed");
					return;
				}

				double delta;

				switch (action) {
					case MotionEvent.ACTION_DOWN:
						delta = 1.0;
						break;
					case MotionEvent.ACTION_UP:
						delta = -1.0;
						break;
					case MotionEvent.ACTION_CANCEL:
						updateMoveCommandStop();
						return;
					default:
						return;
				}

				double angularZDelta = 0.0;
				double linearXDelta = 0.0;

				if (view == mButtonLeft) {
					if (debug) {
						getLog().d("left: delta=" + delta);
					}
					angularZDelta = delta;
				} else if (view == mButtonRight) {
					if (debug) {
						getLog().d("right: delta=" + delta);
					}
					angularZDelta = -delta;
				} else if (view == mButtonForward) {
					if (debug) {
						getLog().d("forward: delta=" + delta);
					}
					linearXDelta = delta;
				} else if (view == mButtonBackward) {
					if (debug) {
						getLog().d("backward: delta=" + delta);
					}
					linearXDelta = -delta;
				} else {
					return;
				}

				updateMoveCommandStepDelta(angularZDelta, linearXDelta);
			}
		});

		return false;
	}

	private void updateMoveCommandVelocity() {
		updateMoveCommand(mAngularZStep, mLinearXStep, mVelocity);
	}

	private void updateMoveCommandVelocity(double velocity) {
		updateMoveCommand(mAngularZStep, mLinearXStep, velocity);
	}

	private void updateMoveCommandStop() {
		updateMoveCommand(0.0, 0.0, mVelocity);
	}

	private void updateMoveCommandStepDelta(double angularZDelta, double linearXDelta) {
		updateMoveCommand(mAngularZStep + angularZDelta, mLinearXStep + linearXDelta, mVelocity);
	}

	private void updateMoveCommand(double angularZStep, double linearXStep, double velocity) {
		if (mConnectedNode == null) {
			return;
		}

		mAngularZStep = angularZStep;
		mLinearXStep = linearXStep;
		mVelocity = velocity;

		mCurTwist.getAngular().setZ(mAngularZStep * mVelocity);
		mCurTwist.getLinear().setX(mLinearXStep * mVelocity);

		if (mRepeatingPublisher != null) {
			mRepeatingPublisher.cancel();
			mRepeatingPublisher = null;
		}

		if (mCurTwist.getAngular().getZ() == 0.0 && mCurTwist.getLinear().getX() == 0.0) {
			// Stop motion.
			mPublisher.publish(mCurTwist);
		} else {
			// Start or continue motion.
			mRepeatingPublisher = new RcaRepeatingPublisher<Twist>(mPublisher, mCurTwist, mFrequency, mConnectedNode.getScheduledExecutorService());
			mRepeatingPublisher.start();
		}
	}

}
