/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.rca_plugin_std.rca_plugin_java.remocomps;

import android.view.View;

import com.github.rosjava.android_remctrlark.rcajava.context.layout.IJavaXmlStringLayoutInflater;
import com.github.rosjava.android_remctrlark.rcajava.error_handling.RcaLayoutException;
import com.github.rosjava.android_remctrlark.rcajava.misc.ICheckedRunnable;
import com.github.rosjava.android_remctrlark.rcajava_impl.AJavaRemoCompImplBase;

import org.jetbrains.annotations.NotNull;
import org.ros.android.MessageCallable;
import org.ros.android.message_processor.MessageProcessor;
import org.ros.android.message_processor.MessageProcessorNodeMain;
import org.ros.namespace.GraphName;

import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

abstract class ARosViewRemoComp extends AJavaRemoCompImplBase {

	public static final String XML_NS_ANDROID = "android";
	protected final Map<String, String> mAndroidAttrs = new HashMap<String, String>();

	protected String readConfig(String defaultViewClass, String paramNameTextViewClass, String viewId) {
		final String viewClass = getConfig().getParamString(paramNameTextViewClass, defaultViewClass);

		// Default attributes which are allowed to be overwritten.
		mAndroidAttrs.put("layout_width", "match_parent");
		mAndroidAttrs.put("layout_height", "match_parent");

		// Read attributes from config.
		final String prefix = getRcaContext().getNamespace().join(XML_NS_ANDROID).toString() + "/";
		final Map<GraphName, Object> params = getConfig().getParams(XML_NS_ANDROID);
		for (Map.Entry<GraphName, Object> entry : params.entrySet()) {
			final String attr = entry.getKey().toString().substring(prefix.length());
			final String value = entry.getValue().toString();
			mAndroidAttrs.put(attr, value);
		}

		// Attributes which are not allowed to be overwritten.
		mAndroidAttrs.put("id", "@+id/" + viewId);

		return viewClass;
	}

	protected String createLayoutXmlString(String viewClass) {
		final StringWriter layout = new StringWriter();
		layout.append("<").append(viewClass).append(" xmlns:").append(XML_NS_ANDROID).append("=\"http://schemas.android.com/apk/res/android\"");
		for (Map.Entry<String, String> entry : mAndroidAttrs.entrySet()) {
			layout.append("\n\t").append(XML_NS_ANDROID).append(":").
					append(entry.getKey()).append("=\"").append(entry.getValue()).append("\"");
		}
		layout.append("/>\n");
		return layout.toString();
	}

	protected View inflateLayoutFromXmlString(String layoutXmlString, String viewId) throws RcaLayoutException {
		if (debug) {
			getLog().d("Inflating layout:\n" + layoutXmlString);
		}
		final IJavaXmlStringLayoutInflater inflater = getLayout().createFromXmlString(layoutXmlString);
		return inflater.findViewByIdName(viewId);
	}

	protected <T, S> void bindViewPropertyToRos(@NotNull MessageProcessorNodeMain<S> messageProcessor,
	                                         @NotNull final View view,
	                                         @NotNull String setterName,
	                                         @NotNull final Class viewPropertyType,
	                                         final MessageCallable<T, S> callable) throws NoSuchMethodException {
		final Method setter = view.getClass().getMethod(setterName, viewPropertyType);
		messageProcessor.setMessageProcessor(new MessageProcessor<S>() {
			@Override
			public void processMessage(S message) {
				// First compute the string on this thread, and then post the result to the UI.
				final T convertedMessage = callable.call(message);
				// It is very important to block this thread until the UI thread is done, otherwise it could happen that
				// message are processed and posted to fast, and the UI thread gets delayed.
				getRcaContext().syncRunOnUiThread(new ICheckedRunnable() {
					@Override
					public void run() throws InvocationTargetException, IllegalAccessException {
						setter.invoke(view, convertedMessage);
					}
				});
			}
		});
	}
}
