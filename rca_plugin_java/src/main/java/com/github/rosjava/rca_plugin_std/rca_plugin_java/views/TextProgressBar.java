/*
 * Copyright (C) 2014 Weavora Blog
 * Source: http://weavora.com/blog/2012/02/23/android-progressbar-with-text/
 *
 * Modified by Denis Graf.
 */
package com.github.rosjava.rca_plugin_std.rca_plugin_java.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ProgressBar;

import com.github.rosjava.rca_plugin_std.rca_plugin_java.R;

public class TextProgressBar extends ProgressBar {

	private String text = "";
	private int textColor = 0xfff3f3f3;
	private float textSize = 15;

	/**
	 * Note: In order to load the style, we have to use the .
	 *
	 * @param actualContext     The layout inflater's context.
	 * @param rcaPluginContext  This view containing RCA plugin context for loading resources.
	 * @param rcaMainAppContext RCA main app's context for loading resources.
	 * @param attrs             XML attributes, or {@code null} if inflating programmatically.
	 * @param defStyle          Style resource id, or 0 if not set.
	 */
	public static TextProgressBar rcaInflate(Context actualContext,
	                                         @SuppressWarnings("UnusedParameters")Context rcaPluginContext,
	                                         @SuppressWarnings("UnusedParameters") Context rcaMainAppContext,
	                                         AttributeSet attrs,
	                                         int defStyle) {
		if (attrs != null) {
			if (defStyle != 0) {
				return new TextProgressBar(actualContext, attrs, defStyle);
			}
			return new TextProgressBar(actualContext, attrs);
		}
		// Set our own default attributes and style by inflating from XML.
		return (TextProgressBar) LayoutInflater.from(actualContext).inflate(R.layout.text_progress_bar, null);
	}

	public TextProgressBar(Context context) {
		super(context);
	}

	public TextProgressBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		setAttrs(attrs);
	}

	public TextProgressBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setAttrs(attrs);
	}

	private void setAttrs(AttributeSet attrs) {
		if (attrs != null) {
			TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.TextProgressBar, 0, 0);
			setText(a.getString(R.styleable.TextProgressBar_text));
			setTextColor(a.getColor(R.styleable.TextProgressBar_textColor, Color.BLACK));
			setTextSize(a.getDimension(R.styleable.TextProgressBar_textSize, 15));
			a.recycle();
		}
	}

	@Override
	protected synchronized void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		Paint textPaint = new Paint();
		textPaint.setAntiAlias(true);
		textPaint.setColor(textColor);
		textPaint.setTextSize(textSize);
		Rect bounds = new Rect();
		textPaint.getTextBounds(text, 0, text.length(), bounds);
		int x = getWidth() / 2 - bounds.centerX();
		int y = getHeight() / 2 - bounds.centerY();
		canvas.drawText(text, x, y, textPaint);
	}

	public String getText() {
		return text;
	}

	public synchronized void setText(CharSequence text) {
		if (text != null) {
			this.text = text.toString();
		} else {
			this.text = "";
		}
		postInvalidate();
	}

	public int getTextColor() {
		return textColor;
	}

	public synchronized void setTextColor(int textColor) {
		this.textColor = textColor;
		postInvalidate();
	}

	public float getTextSize() {
		return textSize;
	}

	public synchronized void setTextSize(float textSize) {
		this.textSize = textSize;
		postInvalidate();
	}
}