/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.rca_plugin_std.rca_plugin_java.remocomps;

import android.widget.TextView;

import com.github.rosjava.android_remctrlark.rcajava.binders.annotations.RcaType;
import com.github.rosjava.android_remctrlark.rcajava.configuration.IJavaRemoCompConfiguration;
import com.github.rosjava.android_remctrlark.rcajava.error_handling.RcaLayoutException;

import org.ros.android.MessageCallable;
import org.ros.android.message_processor.AsyncMessageProcessorNodeMain;
import org.ros.android.message_processor.MessageProcessorNodeMain;
import org.ros.namespace.GraphName;


@RcaType("This RemoComp consists of a text view listening for text messages. The last message is shown in the text view.\n" +

		"\nParameters:\n" +
		"\t" + RosText.PARAM_NAME_VIEW_CLASS + " (str): Sets the class to inflate. " +
		"It must be TextView or a subclass of it. " +
		"Default: " + RosText.PARAM_DVALUE_VIEW_CLASS + "\n" +
		"\t" + RosText.XML_NS_ANDROID + "/<attr> (any): Sets the attribute <attr> of the view to inflate.\n" +

		"\nSubscribed topics:\n" +
		"\t" + RosText.TOPIC_NAME + " (std_msgs.String): Text to display in the text view.")
public class RosText extends ARosViewRemoComp {

	public static final String TOPIC_NAME = "text";
	public static final String PARAM_NAME_VIEW_CLASS = "TextViewClass";
	public static final String PARAM_DVALUE_VIEW_CLASS = "TextView";

	private static final String VIEW_ID = "text_view";

	private MessageProcessorNodeMain<std_msgs.String> mRosNode = null;
	private TextView mView = null;

	@Override
	public void createUI() throws RcaLayoutException {
		final String viewClass = readConfig(PARAM_DVALUE_VIEW_CLASS, PARAM_NAME_VIEW_CLASS, VIEW_ID);
		final String layoutXmlString = createLayoutXmlString(viewClass);
		mView = (TextView) inflateLayoutFromXmlString(layoutXmlString, VIEW_ID);
	}

	@Override
	public void start(IJavaRemoCompConfiguration configuration) throws NoSuchMethodException {
		if (mView == null) {
			// This can happen, if there were errors during creation of the view.
			return;
		}

		mRosNode = new AsyncMessageProcessorNodeMain<String, std_msgs.String>(std_msgs.String._TYPE, TOPIC_NAME, GraphName.of(getRcaContext().getType()));
		bindViewPropertyToRos(mRosNode, mView, "setText", CharSequence.class, new MessageCallable<CharSequence, std_msgs.String>() {
			@Override
			public CharSequence call(std_msgs.String string) {
				return string.getData();
			}
		});

		getNodeExecutor().execute(mRosNode, configuration.createNodeConfiguration(mRosNode.getDefaultNodeName()));
	}

	@Override
	public void shutdown() {
		if (mRosNode != null) {
			getNodeExecutor().shutdownNodeMain(mRosNode);
			mView = null;
			mRosNode = null;
		}
	}
}
