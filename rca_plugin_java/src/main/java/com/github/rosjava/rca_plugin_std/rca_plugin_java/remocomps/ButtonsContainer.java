/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.rca_plugin_std.rca_plugin_java.remocomps;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.github.rosjava.android_remctrlark.rcajava.binders.IJavaRemoCompBinderImpl;
import com.github.rosjava.android_remctrlark.rcajava.binders.annotations.RcaMethod;
import com.github.rosjava.android_remctrlark.rcajava.binders.annotations.RcaParam;
import com.github.rosjava.android_remctrlark.rcajava.binders.annotations.RcaReturn;
import com.github.rosjava.android_remctrlark.rcajava.binders.annotations.RcaType;
import com.github.rosjava.android_remctrlark.rcajava.configuration.IJavaRemoCompConfiguration;
import com.github.rosjava.android_remctrlark.rcajava.misc.ICheckedRunnable;
import com.github.rosjava.android_remctrlark.rcajava_impl.AJavaRemoCompImplBase;
import com.github.rosjava.android_remctrlark.rcajava_impl.error_handling.RosNodeError;

import org.jetbrains.annotations.NotNull;
import org.json.JSONException;
import org.json.JSONObject;
import org.ros.namespace.GraphName;
import org.ros.node.ConnectedNode;
import org.ros.node.Node;
import org.ros.node.NodeMain;
import org.ros.node.topic.Publisher;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@RcaType("This RemoComp creates a container of buttons. Each button can send a message or/and an event when clicked.\n" +

		"\nParameters:\n" +
		"\t" + ButtonsContainer.PARAM_NAME_CMD_ADD_BUTTON + "<id> (str): " +
		"Places a new button with the id <id> into the container. The value specifies the text of the button.\n" +
		"\t" + ButtonsContainer.PARAM_NAME_CMD_ENABLED + "<id> (bool): " +
		"Sets the enabled state of the button with id <id>. True for enabling the button, false otherwise.\n" +
		"\t" + ButtonsContainer.PARAM_NAME_CMD_MESSAGE_ON_CLICK + "<id> (str): " +
		"Sets the message to be sent when button with id <id> is clicked.\n" +
		"\t" + ButtonsContainer.PARAM_NAME_CMD_EVENT_ON_CLICK + "<id> (str): " +
		"Sets the event to be sent to the RemoCon when button with id <id> is clicked.\n" +

		"\nEvents:\n" +
		"\t" + ButtonsContainer.EVENT_NAME + " (JSONObject): Dispatched when buttons are clicked. JSON mappings:\n" +
		"\t\t" + ButtonsContainer.EVENT_DATA_EVENT + " (String): '" + ButtonsContainer.EVENT_NAME_BUTTON_CLICK + "'\n" +
		"\t\t" + ButtonsContainer.EVENT_DATA_BUTTON_ID + " (int): Id of the clicked button.\n" +
		"\t\t" + ButtonsContainer.EVENT_DATA_MESSAGE + " (String): Sent message by the button.\n" +

		"\nPublished topics:\n" +
		"\t" + ButtonsContainer.TOPIC_NAME + " (std_msgs.String): Publishes messages sent when buttons are clicked.")
public class ButtonsContainer extends AJavaRemoCompImplBase {

	public static final String TOPIC_NAME = "message";
	public static final String PARAM_ORIENTATION = "orientation";
	public static final String PARAM_NAME_CMD_ADD_BUTTON = "button_";
	public static final String PARAM_NAME_CMD_ORDER = "order_";
	public static final String PARAM_NAME_CMD_EVENT_ON_CLICK = "event_on_click_";
	public static final String PARAM_NAME_CMD_MESSAGE_ON_CLICK = "message_on_click_";
	public static final String PARAM_NAME_CMD_ENABLED = "enabled_";
	public static final String ORIENTATION_VERTICAL = "vertical";
	public static final String ORIENTATION_HORIZONTAL = "horizontal";
	public static final String EVENT_NAME = "buttons_container_event";
	public static final String EVENT_NAME_BUTTON_CLICK = "button_click";
	public static final String EVENT_DATA_EVENT = "event";
	public static final String EVENT_DATA_BUTTON_ID = "button_id";
	public static final String EVENT_DATA_MESSAGE = "message";

	public static class ButtonIdNotDefinedException extends Exception {
		ButtonIdNotDefinedException(String buttonId) {
			super("No Button specified with id '" + buttonId + "'." +
					"\nPlease add parameter " + PARAM_NAME_CMD_ADD_BUTTON + buttonId + " into your RemoComp configuration.");
		}
	}

	private class Button extends android.widget.Button implements View.OnClickListener {
		public final String mId;
		public Object mOrder;
		public String mEventOnClick = null;
		public String mMessageOnClick = null;

		private Button(Context context, String id) {
			super(context);
			mId = id;
			setOnClickListener(this);
		}

		@Override
		public void onClick(View v) {
			getRcaContext().postToImplThread(new ICheckedRunnable() {

				@Override
				public void run() throws JSONException {
					if (mEventOnClick != null) {
						if (debug) {
							getLog().d("button '" + mId + "' dispatches event: '" + mEventOnClick + "'");
						}

						final JSONObject eventData = new JSONObject();
						eventData.put(EVENT_DATA_EVENT, EVENT_NAME_BUTTON_CLICK);
						eventData.put(EVENT_DATA_BUTTON_ID, mId);
						eventData.put(EVENT_DATA_MESSAGE, mEventOnClick);

						getEventDispatcher().dispatch(EVENT_NAME, eventData);
					}

					if (mMessageOnClick != null) {
						if (debug) {
							getLog().d("button '" + mId + "' sends message: '" + mMessageOnClick + "'");
						}
						assert mPublisher != null;
						final std_msgs.String message = mPublisher.newMessage();
						message.setData(mMessageOnClick);
						mPublisher.publish(message);
					}
				}
			});
		}
	}

	private NodeMain mRosNode = null;
	private Publisher<std_msgs.String> mPublisher = null;
	private Map<String, Button> mButtons = new HashMap<String, Button>();
	private boolean mNeedsRos = false;

	@SuppressWarnings("UnusedDeclaration")
	@Override
	public IJavaRemoCompBinderImpl createBinder() {
		return new IJavaRemoCompBinderImpl() {
			@RcaMethod("Returns the enabled status for this view.")
			@RcaReturn("True if the button is enabled, false otherwise.")
			public boolean isButtonEnabled(
					@RcaParam(name = "buttonId", value = "The id of the button.") String buttonId) throws ButtonIdNotDefinedException {
				return getButton(buttonId).isEnabled();
			}

			@RcaMethod("Sets the enabled state of a button.")
			public void setButtonEnabled(
					@RcaParam(name = "buttonId", value = "The id of the button.") String buttonId,
					@RcaParam(name = "enabled", value = "True for enabling the button, false otherwise.") final Boolean enabled) throws ButtonIdNotDefinedException {
				final Button button = getButton(buttonId);
				getRcaContext().postToUiThread(new ICheckedRunnable() {
					@Override
					public void run() {
						button.setEnabled(enabled);
					}
				});
			}

			@RcaMethod("Sets the enabled state of all buttons.")
			public void setAllButtonsEnabled(
					@RcaParam(name = "enabled", value = "True for enabling the buttons, false otherwise.") final Boolean enabled) {
				getRcaContext().postToUiThread(new ICheckedRunnable() {
					@Override
					public void run() {
						for (Button button : mButtons.values()) {
							button.setEnabled(enabled);
						}
					}
				});
			}
		};
	}

	@Override
	public void createUI() throws ButtonIdNotDefinedException {

		final Map<GraphName, Object> params = getConfig().getParams();
		for (GraphName key : params.keySet()) {
			final String paramBaseName = key.getBasename().toString();
			if (paramBaseName.startsWith(PARAM_NAME_CMD_ADD_BUTTON)) {
				final String buttonId = paramBaseName.substring(PARAM_NAME_CMD_ADD_BUTTON.length());
				final Button button = new Button(getApkContext(), buttonId);
				final String text = getConfig().getParamString(key);
				button.setText(text);
				mButtons.put(buttonId, button);
				if (debug) {
					getLog().d("new button added: buttonId=" + buttonId + ", text='" + text + "'");
				}
			}
		}

		for (final GraphName key : params.keySet()) {
			final String paramBaseName = key.getBasename().toString();
			if (paramBaseName.startsWith(PARAM_NAME_CMD_ORDER)) {
				final String buttonId = paramBaseName.substring(PARAM_NAME_CMD_ORDER.length());
				final Button button = getButton(buttonId);
				button.mOrder = getConfig().getParam(key);
			} else if (paramBaseName.startsWith(PARAM_NAME_CMD_EVENT_ON_CLICK)) {
				final String buttonId = paramBaseName.substring(PARAM_NAME_CMD_EVENT_ON_CLICK.length());
				final Button button = getButton(buttonId);
				button.mEventOnClick = getConfig().getParamString(key);
			} else if (paramBaseName.startsWith(PARAM_NAME_CMD_MESSAGE_ON_CLICK)) {
				final String buttonId = paramBaseName.substring(PARAM_NAME_CMD_MESSAGE_ON_CLICK.length());
				final Button button = getButton(buttonId);
				button.mMessageOnClick = getConfig().getParamString(key);
				mNeedsRos = true;
			} else if (paramBaseName.startsWith(PARAM_NAME_CMD_ENABLED)) {
				final String buttonId = paramBaseName.substring(PARAM_NAME_CMD_ENABLED.length());
				final Button button = getButton(buttonId);
				button.setEnabled(getConfig().getParamBoolean(key));
			}
		}

		final LinearLayout layout = new LinearLayout(getApkContext());
		final List<Button> buttons = new LinkedList<Button>();
		buttons.addAll(mButtons.values());
		Collections.sort(buttons, new Comparator<Button>() {
			@SuppressWarnings("unchecked")
			@Override
			public int compare(Button lhs, Button rhs) {
				assert lhs.getText() != null;
				assert rhs.getText() != null;
				boolean lIsNumber = lhs.mOrder instanceof Number;
				boolean rIsNumber = rhs.mOrder instanceof Number;
				final Object l = lhs.mOrder != null ? lIsNumber ? lhs.mOrder : lhs.mOrder.toString() : lhs.getText().toString();
				final Object r = rhs.mOrder != null ? rIsNumber ? rhs.mOrder : rhs.mOrder.toString() : rhs.getText().toString();

				if (lIsNumber && !rIsNumber) {
					return -1;
				}
				if (!lIsNumber && rIsNumber) {
					return 1;
				}
				return ((Comparable) l).compareTo(r);
			}
		});
		for (Button button : buttons) {
			layout.addView(button);
		}

		if (layout.getChildCount() > 0) {
			final String orientation = getConfig().getParamString(PARAM_ORIENTATION, ORIENTATION_VERTICAL);
			if (orientation.equals(ORIENTATION_HORIZONTAL)) {
				layout.setOrientation(LinearLayout.HORIZONTAL);
				for (Button button : mButtons.values()) {
					final LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT);
					layoutParams.weight = 1;
					button.setLayoutParams(layoutParams);
				}
			} else {
				layout.setOrientation(LinearLayout.VERTICAL);
				for (Button button : mButtons.values()) {
					button.setLayoutParams(new LinearLayout.LayoutParams(
							ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
				}
			}
			getParentView().addView(layout);
		}
	}

	@Override
	public void start(IJavaRemoCompConfiguration configuration) {
		if (!mNeedsRos) {
			return;
		}

		mRosNode = new NodeMain() {
			@Override
			public GraphName getDefaultNodeName() {
				return null;
			}

			@Override
			public void onStart(ConnectedNode connectedNode) {
				mPublisher = connectedNode.newPublisher(TOPIC_NAME, std_msgs.String._TYPE);
			}

			@Override
			public void onShutdown(Node node) {
				getRcaContext().postToImplThread(new ICheckedRunnable() {
					@Override
					public void run() {
						shutdown();
					}
				});
			}

			@Override
			public void onShutdownComplete(Node node) {
			}

			@Override
			public void onError(final Node node, final Throwable throwable) {
				getRcaContext().handleException(new RosNodeError(node, throwable));
			}
		};

		getRcaContext().getNodeExecutor().execute(mRosNode, configuration.createNodeConfiguration(getRcaContext().getType()));
	}

	@Override
	public void shutdown() {
		if (mRosNode != null) {
			getNodeExecutor().shutdownNodeMain(mRosNode);
			mPublisher = null;
			mRosNode = null;
		}
	}

	@NotNull
	private Button getButton(String buttonId) throws ButtonIdNotDefinedException {
		final Button button = mButtons.get(buttonId);
		if (button == null) {
			throw new ButtonIdNotDefinedException(buttonId);
		}
		return button;
	}
}
