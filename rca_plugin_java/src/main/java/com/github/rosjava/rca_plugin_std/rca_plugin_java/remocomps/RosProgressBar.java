/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.rca_plugin_std.rca_plugin_java.remocomps;

import android.widget.ProgressBar;

import com.github.rosjava.android_remctrlark.rcajava.binders.annotations.RcaType;
import com.github.rosjava.android_remctrlark.rcajava.configuration.IJavaRemoCompConfiguration;
import com.github.rosjava.android_remctrlark.rcajava.error_handling.RcaLayoutException;

import org.ros.android.MessageCallable;
import org.ros.android.message_processor.AsyncMessageProcessorNodeMain;
import org.ros.android.message_processor.MessageProcessorNodeMain;
import org.ros.android.message_processor.SimpleMessageProcessorNodeMain;
import org.ros.internal.node.NodeMainGroup;
import org.ros.namespace.GraphName;

import java.lang.String;

import std_msgs.*;

@RcaType("This RemoComp consists of a progress bar view listening for progress status messages. " +
		"The last message is applied to the progress bar view.\n" +

		"\nParameters:\n" +
		"\t" + RosProgressBar.PARAM_NAME_VIEW_CLASS + " (str): Sets the class to inflate. " +
		"It must be a subclass of ProgressBar implementing the method setText(CharSequence). " +
		"Default: " + RosProgressBar.PARAM_DVALUE_VIEW_CLASS + "\n" +
		"\t" + RosProgressBar.XML_NS_ANDROID + "/<attr> (any): Sets the attribute <attr> of the view to inflate.\n" +

		"\nSubscribed topics:\n" +
		"\t" + RosProgressBar.TOPIC_NAME_PROGRESS + " (std_msgs.UInt16): Progress status to be applied to the progress bar." +
		"\t" + RosProgressBar.TOPIC_NAME_TEXT + " (std_msgs.String): Text to display on the progress bar.")
public class RosProgressBar extends ARosViewRemoComp {

	public static final String TOPIC_NAME_PROGRESS = "progress";
	public static final String TOPIC_NAME_TEXT = "text";
	public static final String PARAM_NAME_VIEW_CLASS = "ProgressBarClass";
	public static final String PARAM_DVALUE_VIEW_CLASS = "com.github.rosjava.rca_plugin_std.rca_plugin_java.views.TextProgressBar";

	private static final String VIEW_ID = "progress_bar_view";

	private NodeMainGroup mRosNode = null;
	private MessageProcessorNodeMain<std_msgs.UInt16> mProgressRosNode = null;
	private MessageProcessorNodeMain<std_msgs.String> mTextRosNode = null;
	private ProgressBar mView = null;

	@Override
	public void createUI() throws RcaLayoutException {
		final String viewClass = readConfig(PARAM_DVALUE_VIEW_CLASS, PARAM_NAME_VIEW_CLASS, VIEW_ID);
		final String layoutXmlString = createLayoutXmlString(viewClass);
		mView = (ProgressBar) inflateLayoutFromXmlString(layoutXmlString, VIEW_ID);
	}

	@Override
	public void start(IJavaRemoCompConfiguration configuration) throws NoSuchMethodException {
		if (mView == null) {
			// This can happen, if there were errors during creation of the view.
			return;
		}

		final GraphName defaultNodeName = GraphName.of(getRcaContext().getType());

		mProgressRosNode = new AsyncMessageProcessorNodeMain<Short, std_msgs.UInt16>(std_msgs.UInt16._TYPE, TOPIC_NAME_PROGRESS, defaultNodeName);
		bindViewPropertyToRos(mProgressRosNode, mView, "setProgress", int.class, new MessageCallable<Short, std_msgs.UInt16>() {
			@Override
			public Short call(UInt16 uInt16) {
				return uInt16.getData();
			}
		});

		mTextRosNode = new AsyncMessageProcessorNodeMain<String, std_msgs.String>(std_msgs.String._TYPE, TOPIC_NAME_TEXT, defaultNodeName);
		bindViewPropertyToRos(mTextRosNode, mView, "setText", CharSequence.class, new MessageCallable<CharSequence, std_msgs.String>() {
			@Override
			public CharSequence call(std_msgs.String string) {
				return string.getData();
			}
		});

		mRosNode = new NodeMainGroup(defaultNodeName);
		mRosNode.addNodeMain(mProgressRosNode);
		mRosNode.addNodeMain(mTextRosNode);

		getNodeExecutor().execute(mRosNode, configuration.createNodeConfiguration(defaultNodeName));
	}

	@Override
	public void shutdown() {
		if (mRosNode != null) {
			getNodeExecutor().shutdownNodeMain(mRosNode);
			mView = null;
			mProgressRosNode = null;
			mTextRosNode = null;
			mRosNode = null;
		}
	}
}
