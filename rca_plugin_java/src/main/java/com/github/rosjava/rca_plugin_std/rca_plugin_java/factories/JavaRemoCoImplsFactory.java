/*
 * Copyright (C) 2014 Denis Graf
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.github.rosjava.rca_plugin_std.rca_plugin_java.factories;

import com.github.rosjava.android_remctrlark.rcajava_impl.AJavaRemoCoImplsFactoryBase;
import com.github.rosjava.rca_plugin_std.rca_plugin_java.remocomps.ButtonsContainer;
import com.github.rosjava.rca_plugin_std.rca_plugin_java.remocomps.RosImage;
import com.github.rosjava.rca_plugin_std.rca_plugin_java.remocomps.RosMap;
import com.github.rosjava.rca_plugin_std.rca_plugin_java.remocomps.RosProgressBar;
import com.github.rosjava.rca_plugin_std.rca_plugin_java.remocomps.RosText;
import com.github.rosjava.rca_plugin_std.rca_plugin_java.remocomps.RosVirtualJoystick;
import com.github.rosjava.rca_plugin_std.rca_plugin_java.remocomps.SimpleJoypad;
import com.github.rosjava.rca_plugin_std.rca_plugin_java.remocons.RosTeleop;

@SuppressWarnings("UnusedDeclaration")
public class JavaRemoCoImplsFactory extends AJavaRemoCoImplsFactoryBase {
	@Override
	protected void registerAllRemoCoTypes() {
		register(ButtonsContainer.class);
		register(RosImage.class);
		register(RosMap.class);
		register(RosProgressBar.class);
		register(RosTeleop.class);
		register(RosText.class);
		register(RosVirtualJoystick.class);
		register(SimpleJoypad.class);
	}
}
