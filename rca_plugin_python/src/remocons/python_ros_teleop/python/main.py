# Copyright (C) 2014 Denis Graf
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may not
# use this file except in compliance with the License. You may obtain a copy of
# the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations under
# the License.

import rcapy_impl


class Impl(rcapy_impl.APyRemoConImplBase):
    REMOCOMP_TYPE_MAP = "ros_map"
    REMOCOMP_TYPE_CAMERA = "ros_image"
    REMOCOMP_ID_VISUALIZATION = "visualization"
    BUTTON_ID_SWITCH_VISUALIZATION = "switch_visualization"

    def __init__(self, context):
        """
        @type context: rcapy.APyRemoConContext
        """
        super(Impl, self).__init__(context)
        self._droid = self.context.create_android_proxy()
        self._switch_visualization_to = None

    def start(self, configuration):
        """
        @type configuration: rcapy.APyRemoConConfiguration
        """
        self.parent_view.set_on_click_listener(
            Impl.BUTTON_ID_SWITCH_VISUALIZATION, self.on_click_button_switch_visualization)

        visualization_proxy = self.context.get_remocomp_proxy(Impl.REMOCOMP_ID_VISUALIZATION)
        visualization_proxy.set_on_launched_listener(self.on_remocomp_launched)

    def on_remocomp_launched(self, proxy):
        """
        @type proxy: rcapy.ARemoCompProxy
        """
        if proxy.the_type == Impl.REMOCOMP_TYPE_CAMERA:
            self._switch_visualization_to = Impl.REMOCOMP_TYPE_MAP
            self.parent_view.set_property(Impl.BUTTON_ID_SWITCH_VISUALIZATION, "text", "show map")
        else:
            self._switch_visualization_to = Impl.REMOCOMP_TYPE_CAMERA
            self.parent_view.set_property(Impl.BUTTON_ID_SWITCH_VISUALIZATION, "text", "show camera")

    def on_click_button_switch_visualization(self, name, data):
        """
        @type name: str
        @type data: dict[str, object]
        """
        assert name == "click"
        assert data["id"] == Impl.BUTTON_ID_SWITCH_VISUALIZATION
        if self._switch_visualization_to == Impl.REMOCOMP_TYPE_MAP:
            self.relauncher.set_type(Impl.REMOCOMP_ID_VISUALIZATION, Impl.REMOCOMP_TYPE_MAP)
        else:
            self.relauncher.set_type(Impl.REMOCOMP_ID_VISUALIZATION, Impl.REMOCOMP_TYPE_CAMERA)
        self.relauncher.commit()
